# Mini Project 10: Rust Serverless Transformer Endpoint with AWS Lambda

This project demonstrates the deployment of a large language model (LLM) as a serverless function using AWS Lambda. The function is developed using Rust and Cargo Lambda and provides a local inference endpoint.


## Notice
**Note**: Due to `file size constraints` on Git repositories, the model file `pythia-410m-q4_0-ggjt.bin` is not included in this repository. We use Git Large File Storage (`LFS`) to handle this file and you need to download it directly from [Hugging Face](https://huggingface.co/rustformers/pythia-ggml/blob/main/pythia-410m-q4_0-ggjt.bin) and place it in both the `root directory` and the `/src` directory before building the project.

## Project Goals
- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint

## Steps to Success


### Step 0: LLM Model Selection
For AWS Lambda deployment, we chose the `rustformers/pythia-ggml` model due to its suitable size and the constraints of AWS Lambda's execution duration. Download the pythia-410m-q4_0-ggjt.bin from [here](https://huggingface.co/rustformers/pythia-ggml/blob/main/pythia-410m-q4_0-ggjt.bin) and save this file in the /src directory.

### Step 1: Setup
1. Initialize a new AWS Lambda Rust project with `cargo lambda new <PROJECT_NAME>`.
```bash
cargo lambda new mini_project_10
```

2. Add dependencies to `Cargo.toml`.
```bash
[dependencies]
lambda_http = "0.11.1"
tokio = { version = "1", features = ["macros", "rt-multi-thread"] }
tracing = "0.1.27"
log = "0.4.14"
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
openssl = { version = "0.10.35", features = ["vendored"] }
serde = {version = "1.0", features = ["derive"] }
serde_json = "1.0"
rand = "0.8.5"

```

3. Implement function handlers and inference endpoints in `main.rs`.

4. Test locally with `cargo lambda watch` and the following command:
  ```bash
  curl http://localhost:9000/default/<mini_project10>\?text="Oliver starts working"
  ```

### Step 2: Dockerization and AWS ECR (Elastic Container Registry)
- Configure AWS IAM with necessary policies, including:
    - IAMFullAccess
    - AWSLambda_FullAccess
    - AWSAppRunnerServicePolicyForECRAccess
    - EC2InstanceProfileForImageBuilderECRContainerBuilds
- Naviagte into ECR under AWS console and create a new private repository.
- Associates your local Docker with AWS using:
    ```bash
    aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <AWS-ACCOUNT-NUMBER>.dkr.ecr.us-east-1.amazonaws.com
    ```
- Build and push the Docker image to ECR using:
 ```bash
docker buildx build --progress=plain --platform linux/arm64 -t mini_project10 .
docker tag mini_project_10:latest <AWS-ACCOUNT-NUMBER>.dkr.ecr.us-east-1.amazonaws.com/mini_project_10:latest
docker push <AWS-ACCOUNT-NUMBER>.dkr.ecr.us-east-1.amazonaws.com/mini10:latest
```
###  Step 3: AWS Lambda Deployment
- Nnavigate to Lambda in the AWS console and create a new function with the option Container image, enter the Amazon ECR image URL, and choose arm64 architecture.

- Adjust the function's memory and timeout settings based on your LLM's requirements.

- Enable CORS by creating a function URL:
    - Navigate to your function's configuration page.
    - Select "Function URL" and click "Create function URL".
    - Choose the appropriate method (POST for inference requests).
    - Enable CORS by adding * to "Allow origin" for testing, or specify your domain for production.


## Deliverables
Local test
![local](screenshots/local.png)

ECR
![ECR](screenshots/ECR.png)

AWS Lambda
![Lambda](screenshots/AWS.png)

cURL Request against Endpoint using Insomnia:
- Method: POST
- JSON text: {"text":"Oliver starts working and "}
- URL: https://f2stf6vvzu2iuwkqrur3tjht5u0jpaht.lambda-url.us-east-1.on.aws/ 
- Headers: Content-Type application/json

![Insomnia](screenshots/Insomnia.png)