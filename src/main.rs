// Import necessary libraries
use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use std::convert::Infallible;
use std::io::Write;
use std::path::PathBuf;

// Function to generate response using a language model
fn generate_response(prompt: &str) -> Result<String, Box<dyn std::error::Error>> {
    // Define tokenizer and model architecture
    let tokenizer = llm::TokenizerSource::Embedded;
    let architecture = llm::ModelArchitecture::GptNeoX;
    
    // Define path to the model file
    let model_file = PathBuf::from("pythia-410m-q4_0-ggjt.bin");

    // Load the model
    let model = llm::load_dynamic(
        Some(architecture),
        &model_file,
        tokenizer,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;

    // Start an inference session
    let mut session = model.start_session(Default::default());
    let mut response_text = String::new();

    // Perform inference
    let inference_result = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: prompt.into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(15),
        },
        &mut Default::default(),
        |response| match response {
            // Handle prompt or inferred tokens
            llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
                print!("{token}");
                std::io::stdout().flush().unwrap();
                response_text.push_str(&token);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    );

    // Handle inference result
    inference_result.map_err(Box::new)?;
    Ok(response_text)
}

// Asynchronous function to handle HTTP requests
async fn handle_request(req: Request) -> Result<Response<Body>, Error> {
    // Extract user query from request parameters
    let user_query = req
        .query_string_parameters_ref()
        .and_then(|params| params.first("query"))
        .unwrap_or("Oliver starts working and");

    // Generate response based on user query
    let output_message = match generate_response(user_query) {
        Ok(result) => result,
        Err(e) => format!("Inference error: {:?}", e),
    };
    log::info!("Response from model: {:?}", output_message);

    // Build HTTP response
    let response = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(Body::from(output_message))
        .map_err(|e| format!("Response building error: {:?}", e))?;

    Ok(response)
}

// Asynchronous main function
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Initialize tracing for logging
    tracing::init_default_subscriber();
    // Run the HTTP server
    run(service_fn(handle_request)).await
}
